import java.net.*;
import java.io.*;
public class TcpClient
{
	public static void main(String args[]) throws IOException
	{
		String serverIP = "127.0.0.1";
		Socket clientSocket = new Socket(serverIP, 8080);
		DataInputStream inStream  = new DataInputStream(clientSocket.getInputStream());
		DataOutputStream outStream = new DataOutputStream(clientSocket.getOutputStream());
		byte[] buf = args[0].getBytes();
		byte[] bufLengthInBinary = toBytes(buf.length);
		printBinaryArray(bufLengthInBinary, "here");
		outStream.write(bufLengthInBinary, 0, bufLengthInBinary.length);
		outStream.write(buf, 0, buf.length);
		outStream.flush();
		inStream.readFully(bufLengthInBinary); // ignore the first 4 bytes
		inStream.readFully(buf);
		String ret = new String(buf);
		System.out.println(ret);
	}
	static void printBinaryArray(byte[] b, String comment)
	{
		System.out.println(comment);
		for (int i=0; i<b.length; i++)
		{
			System.out.print(b[i] + " ");
		}
		System.out.println();
	}
	static private byte[] toBytes(int i)
	{
		byte[] result = new byte[4];
		result[0] = (byte) (i >> 24);
		result[1] = (byte) (i >> 16);
		result[2] = (byte) (i >> 8);
		result[3] = (byte) (i /*>> 0*/);
		return result;
	}
}