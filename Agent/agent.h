//
// Created by wendellbest on 2/6/22.
//

#ifndef AGENT_AGENT_H
#define AGENT_AGENT_H

// The following information sent out every minute using UDP
struct BEACON{
    int id; //randomly generated during start up
    int StartUpTime; //the time when the client starts
    int timeInterval; //the time period that beacon will repeat
    char *IP; //the ip address of this client
    int CmdPort; // the client listens to this port for manager commands
    int CmdPortInitial; //stores what the initial CmdPort was
    int CmdPortLimit; //Limit of the CmdPort + 2000 or 65535
    char *serverIP; // server IP address
    int serverUDPPort; // server UDP port
};

void *BeaconSender(void *beacon);
void *CmdAgent(void *beacon);
#endif //AGENT_AGENT_H
