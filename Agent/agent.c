//
// Created by wendellbest on 2/6/22.
//
#include <stdio.h>
#include <sys/utsname.h>
#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include "agent.h"
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <pthread.h>

//This helper function returns the number of digits
//of the number that it is given
int sizeOfNumber(int num){
    int number = num, number2, size = 0;

    number2 = number;
    while(number2 != 0){
        number2 = number2/10;
        size++;
    }

    return size;
}

//This converts a number into a char array
void convertIntToArr(unsigned char *arr, int number, int size){
    int number1 = number, number2 = number;
    for(int i = size - 1; i >=0; i--){
        if(number2 < 10){
            arr[i] = number2 + '0';
        }
        arr[i]= (number2 - (number1 / 10 * 10))+'0';
        number2 = number2 / 10;
        number1 = number1 / 10;
    }
    arr[size] = '\0';
}
//OS[16] contains the local operating system name
// valid = 1 indicates OS is valid
void GetLocalOS(char *OS, int *valid){
    //struct that has the operatingsystem name
    struct utsname getos;
    //Going to set to zero incase the retrieval fails
    *valid = 0;
    //get the operating system name, set the valid bit
    //add a new line character so the string will be sent
    if(uname(&getos)==0){
        *valid = 1;
        strcpy(OS, getos.sysname);
        strcat(OS,"\n");
    }
}

// time contains the current system
// valid = 1 indicates time is valid
void GetLocalTime(int *ostime, int *valid){
    *valid = 0;
    *ostime = time(0);
    *ostime = *ostime;
    if(*ostime != -1){
        *valid = 1;
    }
}

// sends a UDP packet every one minute to the manager
void *BeaconSender(void *beacon){
    //need to cast the void argument to a BEACON
   struct BEACON *bcn = (struct BEACON*)beacon;
   char *finalmessage, *split="-", *id, *startuptime, *interval, *cmdport;
   int agentsocket,  agentIDsize, agentSUPsize, agentintervalsize, agentIPsize, agentportsize;
   struct sockaddr_in manager_addr;
   int manager_struct_length = sizeof(manager_addr);

   //Create the UDP beacon socket
    agentsocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    //Check if successful
    if(agentsocket < 0){
        printf("Unable to create UDP socket\n");
        exit(0);
    }
    else
        printf("UDP socket created successfully\n");

    //Set the info for the manager's IP and TCP port
    manager_addr.sin_family = AF_INET;
    manager_addr.sin_port = htons(bcn->serverUDPPort);
    manager_addr.sin_addr.s_addr = inet_addr(bcn->serverIP);
    //This is for when the agent needs to change the startup time
    //but keep the same ID
    rebuild:
   //get thesizes of the beacon elements in bcn
   agentIDsize = sizeOfNumber(bcn->id);
   agentSUPsize = sizeOfNumber(bcn->StartUpTime);
   agentintervalsize = sizeOfNumber(bcn->timeInterval);
   agentIPsize = strlen(bcn->IP);
   agentportsize = sizeOfNumber(bcn->CmdPort);

   //allocate memory for the char arrays
   id = malloc(agentIDsize*sizeof(char));
   startuptime = malloc(agentSUPsize * sizeof(char));
   interval = malloc(agentintervalsize * sizeof(char));
   cmdport = malloc(agentportsize * sizeof(char));

   //Turn ints to chars
    convertIntToArr(id,bcn->id,agentIDsize);
    convertIntToArr(startuptime,bcn->StartUpTime,agentSUPsize);
    convertIntToArr(interval,  bcn->timeInterval, agentintervalsize);
    convertIntToArr(cmdport, bcn->CmdPort, agentportsize);

    //allocate memory for final message
    finalmessage = malloc((agentIDsize + agentSUPsize + agentintervalsize + agentIPsize + agentportsize + (4 * strlen(split))) * sizeof(char));
   //build final message
    strcat(finalmessage,id);
   strcat(finalmessage, split);
    strcat(finalmessage, startuptime);
    strcat(finalmessage, split);
    strcat(finalmessage, interval);
    strcat(finalmessage, split);
    strcat(finalmessage, bcn->IP);
    strcat(finalmessage, split);
    strcat(finalmessage, cmdport);

    //free the arrays that are no longer needed
    free(id);
    free(startuptime);
    free(interval);
    free(cmdport);
    //printf("The final message is: %s\n", finalmessage);

    //Begin sending beacon
    //interval count is for changing the beacon delay
    int intervalcount = 1;
    //agent count is for making a change in the startup time
    int agentcount = 0;
    while(1){
        //Send the beacon message
        if(sendto(agentsocket, finalmessage, strlen(finalmessage), 0, (struct sockaddr*)&manager_addr, manager_struct_length) < 0){
            printf("Unable to  send message\n");
            perror("Error ");
            break;
        }
        //Using the agent count to keep track of how many packets go out
        //before the startup time is changed
        printf("UDP Beacon %d sent\n", agentcount);
        //this is where the beacon delay is
        sleep(intervalcount);

        agentcount++;

        //This is where the interval is changed. When the interval
        //Exceeds the timeinterval by one, the Manager will think the
        //Agent is dead.When the interval is greater than timeinterval+1
        //It resets to 1.
        if(intervalcount < bcn->timeInterval + 1){
            intervalcount++;
        }
        else
            intervalcount = 1;
        //When the specified number of beacons is sent, the start up time is
        //changed
        if(agentcount == 8){
            //Change start up time
            bcn->StartUpTime = time(0);
            //This changes the CmdPort increasing by one
            //The limit is CmdPort + 2000 and when that
            //is reached the port is set back to initial
            //This is because the program cannot reopen
            //A previously used TCP/IP port, but is necessary
            //when the manager requests and update from the
            //agent when the startup time changes
            if(bcn->CmdPort == bcn->CmdPortLimit){
                bcn->CmdPort = bcn->CmdPortInitial;
            }
            else {
                bcn->CmdPort = bcn->CmdPort + 1;
            }
            agentcount = 0;
            intervalcount = 1;
            //Create a thread to handle the Manager CmdAgent request
            pthread_t cmdagent;
            pthread_create(&cmdagent, NULL, CmdAgent,(void *)bcn);
            //The message needs to be rebuilt with the new startup time and CmdPort
            goto  rebuild;
        }
    }
    //close these ports if loop fails
    close(agentsocket);
    free(finalmessage);
}

//listens to a port that the manager sends commands through TCP
//when command is received, executes command and returns result
//to manager.
void *CmdAgent(void *beacon){
    struct BEACON *bcn = beacon;
    int agentsocket, len, connfd, valid = 0, localtime;
    struct sockaddr_in servaddr, cli;
    char OS[16];

    //Create a new socket
    agentsocket = socket(AF_INET, SOCK_STREAM, 0);
    if(agentsocket < 0){
        printf("CmdAgent socket creation failed\n");
    }
    else
        printf("CmdAgent Socket creation successful\n");
    memset(&servaddr, 0,sizeof(servaddr));

    //Create port to listen on
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(bcn->CmdPort);

    //bind the port to the socket
    if((bind(agentsocket, (struct sockaddr*)&servaddr, sizeof(servaddr))) != 0){
        printf("CmdAgent socket bind failed\n");
    }
    else
        printf("CmdAgent socket bind successful\n");
    //sset the socket to listen
    if((listen(agentsocket, 5))!=0){
        printf("CmdAgent listen failed\n");
    }
    else
        printf("CmdAgent listening\n");

        len = sizeof(cli);
        //wait for input from Manager
        connfd = accept(agentsocket, (struct  sockaddr*)&cli, &len);
        if(connfd < 0){
            printf("CmdAgent incoming request failed\n");
            exit(0);
        }
        else
            printf("CmdAgent incoming request succeded\n");
        //Basically any input from manager will trigger sending the OS
        char buff[256]={0};
        recv(connfd, buff,256,0);
        printf("Read from Manager %s\n",buff);
        printf("Getting OS\n");
        GetLocalOS(OS, &valid);
        //check if getting the OS succeeded
        if(valid == 1){
            write(connfd, OS, sizeof(OS));
            valid = 0;
            printf("Sent OS %s to manger\n", OS);
        }
        else{
            char *failed = "failed";
            write(connfd, failed, sizeof(failed));
            printf("Unable to send OS to manager\n");
        }


        recv(connfd, buff, 256,0);
        printf("Received from manager%s\n", buff);
        printf("Getting local time\n");
        GetLocalTime(&localtime, &valid);
        if(valid == 1){
            int size = sizeOfNumber(localtime);
            char *ltime = malloc(size * sizeof(char));
            convertIntToArr(ltime, localtime, size);
            printf("Time is %s\n",ltime);
            ltime[size] = '\n';
            write(connfd, ltime, size);
            free(ltime);
            printf("Sent local time to manager\n");
        }
        else{
            char *zero = "0";
            write(connfd, zero, sizeof(zero));
            printf("Unable to send local time to manager\n");
        }

        //closing the sockets
        close(connfd);
        close(agentsocket);
}
