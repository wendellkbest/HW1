#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include "agent.h"

/*The purpose of main is to get the IP address of the system the agent
 * will run on, the IP port the system will listen for incoming TCP/IP
 * connection requests, the IP address of the Manager, and the UDP
 * listening port of the manager. I did not hardcode the IP address
 * or port since I wanted the agent to run on any system.
 * */
int main(int args, char *argv[]) {
    struct BEACON *agent = malloc(sizeof(struct BEACON));
    unsigned int one, two, three, four;
    pthread_t beacon, cmdagent;

    if(args == 1){
        fprintf(stderr, "Client IP address, Client port, and Server address and Server port arguments required\n");
        exit(0);
    }
    if(args == 2){
        fprintf(stderr, "Client Port, Server IP address, Server port arguments required\n");
    }
    if(args == 3){
        fprintf(stderr, "Server IP address, Server port arguments required\n");
        exit(0);
    }
    if(args == 4){
        fprintf(stderr, "Server port argument required\n");
        exit(0);
    }

    srand(time(0));
    agent->id = rand();
    agent->StartUpTime = time(0);
    agent->timeInterval = 5;
    agent->IP = strdup(argv[1]);
    agent->CmdPort = atoi(argv[2]);
    agent->CmdPortInitial = agent->CmdPort;
    if((agent->CmdPort + 2000) > 65535)
        agent->CmdPortLimit = 65535;
    else
        agent->CmdPortLimit = agent->CmdPort + 2000;
    agent->serverIP = strdup(argv[3]);
    agent->serverUDPPort = atoi(argv[4]);

    //Create the threads
    pthread_create(&beacon,NULL,BeaconSender,(void*)agent);
    pthread_create(&cmdagent,NULL,CmdAgent,(void*)agent);
    //The joins are used to keep the main program alive so it doesn't shut down the
    // threads when it closes, should never return here unless both threads fail
    pthread_join(beacon,NULL);
    pthread_join(cmdagent, NULL);
    return 0;
}
