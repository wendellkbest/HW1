cmake_minimum_required(VERSION 3.21)
project(TcpServer C)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -pthread")
add_executable(TcpServer
        TcpServer.c)
