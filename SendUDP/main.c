#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <time.h>

struct BEACON{
    int ID;
    int startuptime;
    int interval;
    char *ip;
    int port;
};
//returns size of a number
int sizeOfNumber(int num){
    int number = num, number2 = 0, size = 0;

    number2 = number;
    while(number2 != 0){
        number2 = number2/10;
        size++;
    }

    return size;
}

void convertIntToArr(unsigned char *arr, int number, int size){
    int number1 = number, number2 = number;
    for(int i = size - 1; i >=0; i--){
        if(number2 < 10){
            arr[i] = number2 + '0';
        }
        arr[i]= (number2 - (number1 / 10 * 10))+'0';
        number2 = number2 / 10;
        number1 = number1 / 10;
    }
    arr[size] = '\0';
}

int main(int args, char *argv[]) {
    char *serverIP, *finalmessage, *split ="-", *ID, *start, *inter, *port, *message;
    int socket_desc, serverport, messagesize;
    struct sockaddr_in server_addr;
    int  server_struct_length = sizeof(server_addr);
    struct BEACON *bob = malloc(sizeof(struct BEACON));

    srand(time(0));
    bob->ID = rand();
    bob->startuptime = time(0);
    bob->interval = 60;
    bob->ip = strdup(argv[1]);
    bob->port = atoi(argv[2]);
    serverIP = strdup(argv[3]);
    serverport = atoi(argv[4]);
    messagesize = sizeOfNumber(bob->ID) + sizeOfNumber(bob->startuptime) + sizeOfNumber(bob->interval) + strlen(bob->ip) +
            sizeOfNumber(bob->port) + 1;
    finalmessage = malloc(messagesize * sizeof(char));

    ID = malloc(sizeOfNumber(bob->ID)*sizeof(char));
    convertIntToArr(ID, bob->ID, sizeOfNumber(bob->ID));
    message = malloc(sizeOfNumber(bob->ID)*sizeof(char));
    start = malloc(sizeOfNumber(bob->startuptime) * sizeof(char));
    convertIntToArr(start, bob->startuptime, sizeOfNumber(bob->startuptime));
    inter = malloc(sizeOfNumber(bob->interval) * sizeof(char));
    convertIntToArr(inter, bob->interval, sizeOfNumber(bob->interval));
    port = malloc(sizeOfNumber(bob->port)*sizeof(char));
    convertIntToArr(port, bob->port, sizeOfNumber(bob->port));
    message = malloc((sizeOfNumber(bob->ID)  + strlen(split) + sizeOfNumber(bob->startuptime) + strlen(split) +
            sizeOfNumber(bob->interval) + strlen(split) + strlen(bob->ip)+ strlen(split)+ sizeOfNumber(bob->port))*sizeof(char));
    strcpy(message, ID);
    strcat(message,split);
    strcat(message, start);
    strcat(message, split);
    strcat(message,inter);
    strcat(message,split);
    strcat(message,bob->ip);
    strcat(message,split);
    strcat(message, port);

    printf("%s\n", message);

    socket_desc = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if(socket_desc < 0){
        printf("Error while creating socket\n");
        return -1;
    }
    printf("Socket created successfully\n");
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(serverport);
    server_addr.sin_addr.s_addr = inet_addr(serverIP);

    if(sendto(socket_desc,message,strlen(message), 0, (struct sockaddr*)&server_addr, server_struct_length) < 0){
        printf("Unable to send message\n");
        perror("Error1 :");
        return -1;
    }
    printf("Message sent\n");

    //strcpy(message,"bye");
    //if(sendto(socket_desc, message, strlen(message), 0, (struct sockaddr*)&server_addr, server_struct_length) < 0){
        //printf("Unable to send message\n");
        //perror("Error2 :");
        //return -1;
    //}

    return 0;
}
