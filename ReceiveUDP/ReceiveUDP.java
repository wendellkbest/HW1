import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ReceiveUDP extends Thread {
    public static void main(String[] args) throws IOException{
        int port = Integer.parseInt(args[0]);
        DatagramSocket ds = new DatagramSocket(port);
        byte[] receive = new byte[65535];

        DatagramPacket DpReceive = null;
        while(true){
            DpReceive = new DatagramPacket(receive, receive.length);
            ds.receive(DpReceive);

            System.out.println("Client:-" + data(receive));

            if(data(receive).toString().equals("bye")){
                System.out.println("Client sent bye....EXITING");
                break;
            }
            receive = new byte[65535];
        }
    }

    public static StringBuilder data(byte[] receive) {
        if(receive == null)
            return null;
        StringBuilder ret = new StringBuilder();
        int i = 0;
        while(receive[i] != 0){
            ret.append((char) receive[i]);
            i++;
        }
        return ret;
    }

}
