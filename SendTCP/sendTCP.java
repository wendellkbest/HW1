import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class sendTCP {
    public static void main(String[] args) throws IOException {
        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);
        BufferedReader in=null;
        PrintWriter out=null;


        try {
            Socket kkSocket = new Socket(hostName, portNumber);
            out = new PrintWriter(kkSocket.getOutputStream(),true);
            in = new BufferedReader(new InputStreamReader(kkSocket.getInputStream()));
        }catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String sendTCP = "hello there";
        out.println(sendTCP);

        String receiveTCP;
        receiveTCP = in.readLine();
        System.out.println(receiveTCP);
    }
}
