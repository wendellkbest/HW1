import java.net.DatagramPacket;
import java.time.LocalTime;

public class ProcessData extends Thread{
    private DatagramPacket packet;
    private AgentMonitor agent;
    private String data;
    private  String[] splitString;

    public ProcessData(String receive, AgentMonitor agent){
        this.data = receive;
        this.agent = agent;
    }
    public void run(){
        //Split the string
        splitString = data.split("-",0);
        //Create an AgentData object
        AgentData temp = new AgentData(Integer.parseInt(splitString[0]),Integer.parseInt(splitString[1]),Integer.parseInt(splitString[2]),new String(splitString[3]),Integer.parseInt(splitString[4]));
        //index for search
        int index;
        //see if the Agent already exists
        if((index = agent.findAgentData(temp.getId())) >= 0){
            AgentData present = new AgentData(agent.getAgentData(index));
            //See if the start up times are different
            if(temp.getStartUpTime() != present.getStartUpTime()){
                //Remove the old Agent
                agent.removeAgent(index);
                //copy over the number of packets sent
                temp.setPacketcount(present.getPacketcount());
                // set the present date to the temp data
                present = new AgentData(temp);
                //Local time stamp
                present.setLastReceived(LocalTime.now());
                //increase the packet count
                present.increasePacketCount();
                //Add agent with new start up time
                agent.addAgent(present);
                //Inform the the administrator
                System.out.println("Agent " + present.getId() + " start up time changed");
                //Get OS andlocal time from agent
                CmdAgent cmd = new CmdAgent(present);
                cmd.start();
            }
            else{
                //If it isn't a new agent or the start up time has not changed,
                //Just update the time received.
                temp.setPacketcount(present.getPacketcount());
                present = new AgentData(temp);
                present.setLastReceived(LocalTime.now());
                present.increasePacketCount();
                agent.removeAgent(index);
                agent.addAgent(present);
                System.out.println("Agent ID "+present.getId()+ " update " + present.getPacketcount());
            }
        }
        else{
            //This is for a brand new agent
            //mark when received
            temp.setLastReceived(LocalTime.now());
            //Add to list
            agent.addAgent(temp);
            //carry out commands
            CmdAgent cmd = new CmdAgent(temp);
            cmd.start();
            //notify administrator
            System.out.println("Added new agent " + temp.getId() + " to list.");
        }
    }
}
