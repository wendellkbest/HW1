import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/*Maintains a list of active agents(clients) and checks each one
to see if the last beacon received from agent exceeds 2 times its
time interval specified in it's beacon, prints out an alert message.
*/

public class AgentMonitor extends Thread{
    private List<AgentData> agentdatalist = new ArrayList<>();
    //Methods used largely by ProcessData class to add, remove, and find agents
    public void addAgent(AgentData obj){agentdatalist.add(obj);}
    public AgentData getAgentData(int index){ return agentdatalist.get(index);}
    public void  removeAgent(int index){agentdatalist.remove(index);}
    //returns the index of a matching ID
    public int findAgentData(int id){
        int result = -1;
            for(int i = 0; i < agentdatalist.size(); i++){
                if(agentdatalist.get(i).getId() == id){
                    result = i;
                    break;
                }
            }
        return result;
    }

    @Override
    public void run() {
        System.out.println("Running Agent Monitor");
        while(true){
            //System.out.println("Monitor loop Running");
            for(int i = 0; i < agentdatalist.size();i++){

                //retreive the times
                LocalTime lastBeacon = agentdatalist.get(i).getLastReceived();
                LocalTime current = LocalTime.now();

                //convert the times into an int
                int lastB = lastBeacon.getHour() * 60 * 60 + lastBeacon.getMinute() * 60 + lastBeacon.getSecond();
                int cur = current.getHour() * 60 * 60 + current.getMinute() * 60 + current.getSecond();
                //Subtract current time from the time step
                int result = cur - lastB;
                //Test to see if the interval time has been exceeded and if the agent was formerly alive
                if((result > agentdatalist.get(i).getTimeInterval()) && (agentdatalist.get(i).isItDead() == 0)){
                    System.out.println("Agent " + agentdatalist.get(i).getId() + " is dead.");
                    //ID agent as dead,and keeps the message from being sent out again
                    agentdatalist.get(i).setToDead();
                }
                //If an existing agent has a good interval time and is currently considered dead, it's status is changed
                else if((agentdatalist.get(i).isItDead() == 1) && (result < agentdatalist.get(i).getTimeInterval())){
                    System.out.println("Agent "+ agentdatalist.get(i).getId() + " has returned.");
                    agentdatalist.get(i).seToAlive();
                }
            }
            try {
                //no need to run the thread constantly I think
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
