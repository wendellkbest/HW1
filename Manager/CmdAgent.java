import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Scanner;


public class CmdAgent extends Thread{
    private AgentData agent;
    private PrintWriter out = null;
    private Socket socket;
    private Scanner in;
    private DataInputStream dis;

    public CmdAgent(AgentData data) {
        this.agent = new AgentData(data);
    }
    public void run() {
        /* Issues two commands, GetLocalOS and GetLocalTime
        and prints out the results. Needs to establish a TCP
        connection then send commands to client, then receive
        those commands.
        */
        System.out.println("The CMD Agent Ran");
        //Creating the socket
        try {
            socket = new Socket(agent.getIp(), agent.getCmdPort());
            out = new PrintWriter(socket.getOutputStream());
            in = new Scanner(new BufferedInputStream(socket.getInputStream()));
            dis = new DataInputStream(socket.getInputStream());

            String getos = "GetLocalOS";
            out.println(getos);
            out.flush();
            System.out.println("Sent OS request");
            String receivedos;
            receivedos = in.nextLine();
            System.out.println("Received os: " + receivedos);

            String getlocaltime = "GetLocalTime";
            System.out.println("Requesting local time of Agent");
            out.println(getlocaltime);
            out.flush();
            System.out.println("Sent local time request");

            byte[] reply = new byte[50];
            int bytesRead = dis.read(reply);
            byte[] slice = Arrays.copyOfRange(reply, 0, bytesRead);
            int response = ByteBuffer.wrap(slice).order(ByteOrder.LITTLE_ENDIAN).getInt();

            System.out.println("Received local time from Agent");

            System.out.println("Operating system is " + receivedos + " time is " + response);

            //closing all sockets
            out.close();
            socket.close();
            in.close();
            dis.close();
        } catch (UnknownHostException u) {
            u.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
