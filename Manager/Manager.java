/* The main program to start Manager. It receives the port for the
Manager to listen on a UDP port when it launches BeaconListener.
Creates a BeaconListener and AgentMonitor threads.
* */
public class Manager {
    public static void main(String args[]){
        int port = 0;

        //get the port from the command line
        if(args.length == 0){
            System.out.println("Port number argument required");
            return;
        }
        else{
            port = Integer.parseInt(args[0]);
        }
        //Contains the list of agents
        AgentMonitor agent = new AgentMonitor();
        //Runs the beacon and also updates the agent list in agent monitor
        BeaconListener bcnlistener = new BeaconListener(agent,port);
        //Start the threads
        System.out.println("Starting agent");
        agent.start();
        System.out.println(("Starting beacon listener"));
        bcnlistener.start();
    }
}
