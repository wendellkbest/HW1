import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/* Listens to a UDPport for beacons. When receiving a beacon,
checks to see if the beacon is from a new agent,  if it is a
new agent it prints a message to administrator. A new agent
is one that isn't in the list or the startup time has changed.
Also will update the last time a beacon was received.
* */
public class BeaconListener extends Thread{
    private AgentMonitor agent;
    private DatagramSocket socket;
    private byte[] buf = new byte[65535];
    int port;

    public BeaconListener(AgentMonitor agent, int port) {
        this.agent = agent;
        this.port = port;
    }
    //The beacon listener
    public void run(){
        System.out.println("Running Beacon Listener on port " + port);
        //Create the socket
        try {
            socket = new DatagramSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        //Wait for packets
        while(true){
            DatagramPacket packet = new DatagramPacket(buf,buf.length);
            //receive packet
            try {
                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //convert data to string for processing
            String received = new String(data(buf));
            System.out.println("Agent sent: " + data(buf));
            //If a packet is received, then process it
            if(received != null){
                //System.out.println("Received UDP packet from Agent");
                ProcessData job = new ProcessData(received, agent);
                job.start();
            }
            //reset buffer for new packet
            buf = new byte[65535];
        }
    }
    //Function for creating a string from byte array, thank you professor
    public static StringBuilder data(byte[] receive){
        if(receive == null)
            return null;
        StringBuilder ret = new StringBuilder();
        for(int i = 0; receive[i]!= 0; i++){
            ret.append((char) receive[i]);
        }
        return ret;
    }
}
