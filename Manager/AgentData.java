import java.time.LocalTime;

public class AgentData {
    private int id; //From the BEACON
    private int startUpTime; //From the BEACON
    private int timeInterval; //From the BEACON
    private String ip; //From the BEACON
    private int cmdPort; //From the BEACON
    private LocalTime lastReceived; //Used to keep track fo when last beacon received
    private int isDead; //Mark agent as dead so no further messages are required
    private int packetcount; //Keep track of number of packets sent by the ID

    public AgentData(int id, int startuptime, int timeinterval, String ip, int cmdPort){
        this.id = id;
        this.startUpTime = startuptime;
        this.timeInterval = timeinterval;
        this.ip= ip;
        this.cmdPort = cmdPort;
        isDead = 0;
        packetcount = 0;
    }
    public AgentData(AgentData original){
        this.id = original.id;
        this.startUpTime = original.startUpTime;
        this.timeInterval = original.timeInterval;
        this.ip=original.ip;
        this.cmdPort = original.cmdPort;
        isDead = original.isDead;
        packetcount = original.packetcount;
    }
    //Self-explanatory
    public int getId() {return id;}

    public int getStartUpTime() {return startUpTime;}

    public int getTimeInterval() {return timeInterval;}

    public String getIp() {return ip;}

    public int getCmdPort() {return cmdPort;}

    public void setLastReceived(LocalTime time){this.lastReceived=time;}

    public LocalTime getLastReceived() { return lastReceived;}

    public void setToDead(){isDead = 1;}

    public void seToAlive(){isDead = 0;}

    public int isItDead(){return isDead;}

    public int getPacketcount(){return packetcount;}

    public void increasePacketCount(){packetcount++;}

    public void setPacketcount(int number){packetcount = number;}
}
