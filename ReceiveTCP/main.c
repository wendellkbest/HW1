#include <stdio.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#define MAX 80
#define SA struct sockaddr

int main(int args, char *argv[]){
    int sockfd, connfd, len, port;
    struct sockaddr_in servaddr, cli;

    port = atoi(argv[1]);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd == -1){
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("socket successfully created..\n");

    memset(&servaddr, 0, sizeof(servaddr));

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(port);

    if((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0){
        printf("Socket bind failed..\n");
        exit(0);
    }
    else
        printf("Socket successfully binded..\n");

    if((listen(sockfd, 5)) != 0){
        printf("Listen failed..\n");
        exit(0);
    }
    else
        printf("Server listening..\n");

    len = sizeof(cli);

    connfd = accept(sockfd, (SA*)&cli, &len);
    if(connfd < 0){
        printf("Server accept failed..\n");
        exit(0);
    }
    else
        printf("server accept the client..\n");
    char buff[MAX]={0};
    read(connfd, buff, MAX);
    printf("%s\n", buff);
    char *buff2 = "received";
    write(connfd, buff2, 8);
    close(sockfd);
}
